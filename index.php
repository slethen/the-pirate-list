<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>The Pirate List</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/flag-icon.min.css">

   <?php

    if(isset($_GET['no-js'])){
       echo '<link rel="stylesheet" href="css/no-js.css">';
    }else{
       echo '<noscript>
              <meta http-equiv="refresh" 
              content="0; url=/?no-js">
            </noscript>
            <link rel="stylesheet" href="css/the-pirate-list.css">
            <script type="text/javascript" src="js/jquery.min.js" charset="UTF-8"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>';
    }

    ?>

  </head>

  <body>

    <div class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">The Pirate List</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="#">Alternate methods</a></li>
            <li><a href="#">Setup a Proxy</a></li>
            <li><a href="#">Submit a Proxy</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

<center><div class="container-fluid">
  <div class="page-header">
    <h1>View Database</h1>
  </div>

<div class="row row-centered table-responsive">

<?php

$json=file_get_contents("http://153.92.127.120/proxies/api.php");
$data =  json_decode($json);
echo '<table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th><h4>URL</th>
            <th><h4>COUNTRY</th>
            <th><h4>TIME</th>
          </tr>
        </thead>';

foreach($data as $object):

?>

<tbody>
  <tr style="cursor:pointer;" onclick="window.location.href = 'https://<?php echo $object->{'URL'}?>';">
    <td><?php echo $object->{'URL'}?></td>
    <td><span class="flag-icon flag-icon-<?php echo strtolower($object->{'COUNTRY'})?>"></span></td>
    <td><?php echo substr($object->{'TIME'},0,5)?></td>
  </tr>
</tbody>

<?php endforeach; echo "</table>";?>

    </div><!-- /.container -->
    </div>
    </center>
  </body>
</html>